<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>

        
    </head>
    <body >
        

        <div class="container" id="app">
            <div class="row mt-5">
                <h1 class="text-center">Productos</h1>
                <div class="d-grid gap-2 col-3 mx-auto">
                    <button v-if="showaction==='list'" v-on:click="actionAdd" type="button" class="btn btn-primary my-2" > <i class="fa fa-plus-square-o" aria-hidden="true"> </i>  Agregar</button>
                    <button v-if="showaction==='add' || showaction==='edit'" v-on:click="actionList" type="button" class="btn btn-primary my-2" > <i class="fa fa-plus-square-o" aria-hidden="true"> </i>  Listado</button>
                </div>
                <div class="list-group" v-if="showaction==='list'" >
                    <div  class="list-group-item li" v-for="item in list" v-bind:key="item.id" aria-current="true">
                      <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1">@{{item.name}}</h5>
                        <small>
                            
                               <button v-on:click="actionEdit(item)" type="button" > <i class="fa fa-pencil-square-o" aria-hidden="true"> </i>  Editar</button>
                            
                        </small>
                      </div>
                      <p class="mb-1">@{{item.detail}}</p>
                      <small>
                        <button v-on:click="actionDel(item)" type="button">  <i class="fa fa-trash-o fa-lg"></i> </small> Eliminar </button>
                    </div>              

                </div>
                <div class="list-group" v-if="showaction==='edit'" >
                    <div class="alert alert-danger" v-if="error" role="alert">
                        Por Favor Verifique todo los campos son obligatorios
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nombre</label>
                        <input type="text" v-model="info.name" class="form-control" id="exampleFormControlInput1" placeholder="Nombre">
                        <span class="text-danger" v-if="error?.name">@{{error.name[0]}}</span>
                    </div>
                    <div class="mb-3">
                       <label for="exampleFormControlTextarea1" class="form-label">Descripción</label>
                       <textarea  v-model="info.detail" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descripción" ></textarea>
                       <span class="text-danger" v-if="error?.detail">@{{error.detail[0]}}</span>
                    </div>

                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button v-on:click="sendEdit" class="btn btn-primary me-md-1"  type="button">Modificar</button>
                    </div>
                </div >
                <div class="list-group mt-5" v-if="showaction==='add'" >
                    <div class="alert alert-danger" v-if="error" role="alert">
                        Por Favor Verifique todo los campos son obligatorios
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nombre</label>
                        <input type="text" v-model="info.name" class="form-control" id="exampleFormControlInput1" placeholder="Nombre">
                        <span class="text-danger" v-if="error?.name">@{{error.name[0]}}</span>
                    </div>
                    <div class="mb-3">
                       <label for="exampleFormControlTextarea1" class="form-label">Descripción</label>
                       <textarea  v-model="info.detail" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descripción" ></textarea>
                       <span class="text-danger" v-if="error?.detail">@{{error.detail[0]}}</span>
                    </div>

                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button v-on:click="sendAdd" class="btn btn-primary me-md-1"  type="button">Agregar</button>
                    </div>
                </div>
                <div class="list-group" v-if="showaction==='del'" >
                    <h6>Confirmar la eliminación del producto</h6>
                    <b>Z@{{infoDelete.name}}</b>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button v-on:click="sendDel" class="btn btn-danger me-md-1"  type="button">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/65e7c06a46.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
        
        <script>
            
            var app = new Vue({
                el: '#app',
                data: {
                    message: 'Hello Vue!',
                    showaction: 'list',
                    list: null,
                    infoDelete: null,
                    info:{
                        name: null,
                        detail: null
                    },
                    error:false
                },
                created: function () {
                    this.sendList();
                },
                methods: {
                    actionList(){
                        this.showaction= 'list';
                        this.sendList();
                    },
                    actionEdit(item){
                        this.showaction= 'edit';
                        this.info = item;
                        this.error = false;
                    },
                    actionAdd(){
                        this.error = false;
                        this.info = {
                            name : null,
                            detail: null
                        }
                        this.showaction= 'add';
                    },
                    actionDel(item){
                        this.showaction= 'del';
                        this.infoDelete = item;
                    },
                    actionShow(){
                        this.showaction= 'show';
                    },
                    sendList(){      
                        var vm = this;                  
                        axios.get('/api/products')
                        .then(function (response) {
                            // handle success
                            console.log(response);
                            vm.list = response.data.data;
                        })
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                        })
                        .then(function () {
                            // always executed
                        });
                    },
                    sendAdd(){
                        var data = this.info;
                        var vm = this;
                        axios.post('/api/products', data)
                        .then(function (response) {
                            console.log(response.data);
                            vm.actionList();
                        })
                        .catch(function (error) {
                            console.log(error);
                            vm.error = true;
                        });

                    },
                    sendDel(){
                                               
                        var infoDelete = this.infoDelete.id;
                        var vm = this;
                        axios.delete('/api/products/'+infoDelete)
                        .then(function (response) {
                            console.log(response.data);
                            vm.actionList();
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                    },
                    sendEdit(){
                        var data = {
                            name: this.info.name,
                            detail: this.info.detail
                        };

                        var id= this.info.id;
                        var vm = this;
                        axios.put('/api/products/'+id, data)
                        .then(function (response) {
                            console.log(response.data);
                            vm.actionList();
                        })
                        .catch(function (error) {
                            console.log(error);
                            vm.error = true;
                        });
                    }

                }
            })
        </script>
    </body>
</html>
